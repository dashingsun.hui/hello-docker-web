package com.roger.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/")
public class DemoController {
    @GetMapping
    @RequestMapping("")
    public String demo(){
        return "hello world rest";
    }
}
