FROM 192.168.23.103:5000/alaudaorg/tomcat:8.5.29-jre8

ADD docker-demo.war /usr/local/tomcat/webapps/

### run ###
EXPOSE 31111

CMD ["catalina.sh", "run"]